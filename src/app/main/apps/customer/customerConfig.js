import {authRoles} from 'app/auth';
import CustomerTable from './customer';

export const CustomerConfig = {
    settings: {
        layout: {
            config: {
                navbar        : {
                    display: true
                },
                toolbar       : {
                    display: true
                },
                footer        : {
                    display: true
                },
                leftSidePanel : {
                    display: true
                },
                rightSidePanel: {
                    display: true
                }
            }
        }
    },
    auth    : authRoles.onlyGuest,
    routes  : [
        {
            path     : '/customer',
            component: CustomerTable
        }
    ]
};

