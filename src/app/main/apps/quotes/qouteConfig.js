import {authRoles} from 'app/auth';
import QuoteTable from './quote'
export const QuoteConfig = {
    settings: {
        layout: {
            config: {
                navbar        : {
                    display: true
                },
                toolbar       : {
                    display: true
                },
                footer        : {
                    display: true
                },
                leftSidePanel : {
                    display: true
                },
                rightSidePanel: {
                    display: true
                }
            }
        }
    },
    auth    : authRoles.onlyGuest,
    routes  : [
        {
            path     : '/quote',
            component: QuoteTable
        }
    ]
};

